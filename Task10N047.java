/*
Написать рекурсивную функцию для вычисления k-го члена последовательности Фибоначчи. Последовательность Фибоначчи 12 , , ... ff образуется по закону: 1 1; f 2 1; f 12 i i i f f f (i 3, 4, ...).
 */
package Task10N047;
import java.util.Scanner;

public class Task10N047 {
  public static void main(String[] args) {

    System.out.print("Bвод номера k-го члена последовательности Фибоначчи - ");
    Scanner sr = new Scanner(System.in);
    int k = sr.nextInt();
    if (k>0)
    System.out.println("k-ый член последовательности Фибоначчи равен "+result(k));
    else
    System.out.println("Не верный ввод значений");
  }
  public static int result (int k) {
      if (k == 0) return 0;
      if (k == 1) return 1;
      return (result(k - 1) + result(k - 2));
  }
}

