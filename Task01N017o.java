/*
Записать по правилам изучаемого языка программирования следующие вы-
ражения
*/
package Task01N017;
import java.util.Scanner;

public class Task01N017o {

    public static void main (String[] args) {
        System.out.println("Решение уравнения вида y=sqrt(1-(sin(x))^2)");
        Scanner k = new Scanner(System.in);
        System.out.println ("Введите значения x, целое число: ");
        int x = k.nextInt();
        double y = Math.sqrt(1 - Math.pow(Math.sin(x), 2));

        System.out.print("Ответ y = "+y);
    }
}