/*
Дано предложение. Поменять местами его первое и последнее слово.
 */
package Task09N166;

public class Task09N166 {
  public static void main(String[] args) {
    String tmp, s = "Дано предложение Поменять местами его первое и последнее слово";
    String[] words = s.split(" ");
    tmp = words[0];
        // System.out.println(tmp);
    words[0] = words[words.length-1];
        // System.out.println(words[0]);
    words[words.length-1] = tmp;
        // System.out.println(words[words.length-1]);
    for (String word : words) {
      System.out.print(word + " ");
    }
  }
}
