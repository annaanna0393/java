package Task12N234;
import java.util.Scanner;

public class Task12N234b {
  public static void main (String args []) {
    Scanner sr = new Scanner(System.in);
    System.out.println("Ввод n - колличества строк и m - колличества столбцов: ");
    int n = sr.nextInt(); //строки
    int m = sr.nextInt(); //столбцы
    int matr[][] = new int[n][m];
    int i, j, k = 0;
    for (i = 0; i < n; i++)
      for (j = 0; j < m; j++) {
        matr[i][j] = k;
        k++;
      } // Для проверки работы создано автозаполнение.

    for (i = 0; i < n; i++) {
      for (j = 0; j < m; j++) {
        System.out.print(matr[i][j] + "\t");
      }
      System.out.println();
    }

    System.out.println("Ввод s - номера столбца для удаления: ");
    int s = sr.nextInt();
    if (s <=m&&s>=1){
      for (i = 0; i < n; i++)
        for (j = 0; j < m; j++)
          if (j == s - 1) {
            for (j = s - 1; j < m; j++) {
              int result = 0;
              if (j < m - 1) {
                result = matr[i][j + 1];
                matr[i][j] = result;
              } else
                matr[i][j] = result;
            }

          }

      for (i = 0; i < n; i++) {
        for (j = 0; j < m; j++) {
          System.out.print(matr[i][j] + "\t");
        }
        System.out.println();
      }
    }
    else System.out.println("Такого столбца не существует");

  }
}