/*
Дано натуральное число. а) Верно ли, что оно заканчивается четной цифрой?
 */
package Task04N033;
import java.util.Scanner;

public class Task04N033a {
  public static void main(String args[]) {
    Scanner scanner = new Scanner(System.in);
    System.out.println("Введите число, которое заканчивается чётной цифрой: ");
    int a = scanner.nextInt();

    a = (a%10)%2;
    if (a==0) {

      System.out.println("Истина");
    }
    else {
      System.out.println("Ложь");
    }
  }
}