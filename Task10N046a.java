/*
Даны первый член и знаменатель геометрической прогрессии. Написать рекурсивную функцию: а) нахождения n-го члена прогрессии;
 */
package Task10N046;
import java.util.Scanner;

public class Task10N046a {
  public static void main(String [] args){

    System.out.print("Bвод первого члена, знаменателя и номера n-го члена геометрической прогрессии - ");
    Scanner sr = new Scanner(System.in);
    int a = sr.nextInt();
    int q = sr.nextInt();
    int n = sr.nextInt();
    if (q==0|n==0)
    {System.out.println("Не верный ввод значений");}
    else
    {System.out.println("n-ый член прогрессии равен "+result(q, a, n-1));}

  }
  public static int result (int q, int a, int n) {

    if (n==0)
      return a;
    else {
      return q*result (q, a, n-1);
    }

  }
}