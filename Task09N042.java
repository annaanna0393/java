/*
Составить программу, которая печатает заданное слово, начиная с последней
буквы.
 */
package Task09N042;

public class Task09N042 {
  public static void main (String [] args) {
    String s = "Определить";
    StringBuilder builder = new StringBuilder();
    for (int i = s.length(); i > 0 ; i--)
    {
      builder.append(s.charAt(i-1));
    }
    System.out.println(builder);
  }
}
