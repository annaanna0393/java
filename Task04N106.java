/*
Составить программу, которая в зависимости от порядкового номера дня месяца (1, 2, ..., 12) выводит на экран время года, к которому относится этот месяц.
 */
package Task04N106;
import java.util.Scanner;

public class Task04N106 {
  public static void main (String args []){

    Scanner scanner = new Scanner(System.in);
    System.out.println("Введите k-й месяц года: ");

    int m = scanner.nextInt();

    switch (m) {
      case 1: System.out.print("1-й месяц Январь");
        break;
      case 2: System.out.print("2-й месяц Февраль");
        break;
      case 3: System.out.print("3-й месяц Март");
        break;
      case 4: System.out.print("4-й месяц Апрель");
        break;
      case 5: System.out.print("5-й месяц Май");
        break;
      case 6: System.out.print("6-й месяц Июнь");
        break;
      case 7: System.out.print("7-й месяц Июль");
        break;
      case 8: System.out.print("8-й месяц Август");
        break;
      case 9: System.out.print("9-й месяц Сентябрь");
        break;
      case 10: System.out.print("10-й месяц Октябрь");
        break;
      case 11: System.out.print("11-й месяц Ноябрь");
        break;
      case 12: System.out.print("12-й месяц Декабрь");
        break;
      default: System.out.print("Не верный ввод условий");
    }

  }
}