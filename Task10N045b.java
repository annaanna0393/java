/*
 Даны первый член и разность арифметической прогрессии. Написать рекурсивную функцию для нахождения:
 б) суммы n первых членов прогрессии
 */
package Task10N045;
import java.util.Scanner;

public class Task10N045b {
  public static void main(String [] args){

    System.out.print("Bвод первого члена, разности и номера n-го члена арифметической прогрессии - ");
    Scanner sr = new Scanner(System.in);
    int a = sr.nextInt();
    int d = sr.nextInt();
    int n = sr.nextInt();
    if (d==0|n==0)
    {System.out.println("Не верный ввод значений");}
    else
    {System.out.println("сумма n первых членов прогрессии равна "+result(d, a, n));}
  }
  public static int result (int d, int a, int n) {
    int an = a;
    int sum = a;
    while ((n - 1) != 0) {
      an += d;
      sum += an;
      n--;
    }
    return sum;
  }
}