package Task11N158;
import java.util.HashMap;
import java.util.Random;
import java.util.Scanner;

public class Task11N158 {
  public static void main(String[] args) {
    Scanner sr = new Scanner(System.in);
    System.out.println("Ввод n - размера массива: ");
    int n = sr.nextInt();
    int[] values = new int[n];
    int [] sB = new int [values.length];
    HashMap<Integer, Integer> shMap  = new HashMap<>();
    Random rnd = new Random();


    for (int i = 0; i < values.length; i++) {
      values[i] = rnd.nextInt(10);
    }//заполнение массива


    for (int i = 0; i < values.length; i++) {
      System.out.print(values[i] + " ");
    }
    System.out.println();//вывод исходного массива


    for (int i = 0; i < values.length; i++)
      shMap.put(values [i], 0);


    int j=0;
    for (int i = 0; i < values.length; i++) {
      if( shMap.get(values[i]).equals(0)) {
        shMap.put(values[i], values[i]);
        sB[j++] = values[i];
      }
    }


    for (int i = 0; i < sB.length; i++) {
      System.out.print(sB[i] + " ");
    }//вывод нового массива


  }
}