package Task12N023;

import java.util.Scanner;

public class Task12N023c {

  public static void main (String args []) {
    Scanner sr = new Scanner(System.in);
    System.out.println("Ввод n - размера массива: ");
    int n = sr.nextInt();
    int m[][] = new int[n][n];
    int i, j;
    int k = 1;
    int l = -1;
    int q = n;
    if (n % 2 == 1) {
      for (i = 0; i < n; i++) {
        l++;
        q--;
        for (j = 0; j < n; j++)
          if (j >= l && j <= q) {
            {
              m[i][j] = k;
            }
          } else if (j >= q && j <= l) {
            {
              m[i][j] = k;
            }
          }
      }
      for (i = 0; i < n; i++) {
        for (j = 0; j < n; j++) {
          System.out.print(m[i][j] + "\t");
        }
        System.out.println();
      }
    }
    else System.out.println("Ошибка начального условия");
  }
}