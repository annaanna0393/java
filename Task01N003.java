/*
Составить программу вывода на экран числа, вводимого с клавиатуры. Выво-
димому числу должно предшествовать сообщение "Вы ввели число".
*/
package Task01N003;
import java.util.Scanner;

public class Task01N003 {

    public static void main (String[] args) {
        System.out.println("Введите целое число");
        Scanner k = new Scanner(System.in);
        int x = k.nextInt();
        System.out.print("Вы ввели число "+x);
    }
}