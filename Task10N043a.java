/*
Написать рекурсивную функцию: а) вычисления суммы цифр натурального числа;
 */
package Task10N043;
import java.util.Scanner;

public class Task10N043a {
  public static void main(String [] args){
    int n;
    System.out.print("Bвод натурального числа - ");
    Scanner sr = new Scanner(System.in);
    n = sr.nextInt();

    System.out.println("Сумма цифр в числе равна "+sum(n));
  }
  public static int sum (int n) {

    if (n < 10) {
      return n;
    }
    else {
      return n % 10 + sum(n / 10);
    }

  }
}
