/*
Даны целые числа h, m, s (0 < h ≤ 23, 0 ≤ m ≤ 59, 0 ≤ s ≤ 59), указывающие
момент времени: "h часов, m минут, s секунд". Определить угол (в градусах)
между положением часовой стрелки в начале суток и в указанный момент
времени.
 */
package Task02N039;
import java.util.Scanner;

public class Task02N039 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите целые числа 0 < h ≤ 23 часов, 0 ≤ m ≤ 59 минут, 0 ≤ s ≤ 59 секунд: ");
        int h = scanner.nextInt();
        int m = scanner.nextInt();
        int s = scanner.nextInt();

        if (h<=23&&h>0&&m>=0&&m<=59&&s>=0&&s<=59) {
            if (h >= 12)  {
                h -= 12;
                double x = h * 30 + 0.5 * m + s * 0.5 / 60;
                System.out.print("Ответ: " + x);
            }
            else {
                double x = h * 30 + 0.5 * m + s * 0.5 / 60;
                System.out.print("Ответ: " + x);
            }
        }
        else {
            System.out.print("Не верный ввод условий");
        }

    }
}
