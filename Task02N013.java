/*
Дано трехзначное число. Найти число, полученное при прочтении его цифр
справа налево
*/
package Task02N013;
import java.util.Scanner;

public class Task02N013 {

    public static void main(String[] args) {
        Scanner k = new Scanner(System.in);
        System.out.println ("Введите целое число от 100 до 200: ");
        int n = k.nextInt();// заданное число
        int m = 0;
        if (n>100&&n<200) {

                for (int i = 0; i < 3; i++) {
                    m *= 10;
                    m += n % 10;
                    n /= 10;
                }
                System.out.print("При прочтении цифр справа налево "+m);

        }
        else {
            System.out.println ("Число не соответствует требованиям");
        }
    }
}