/*
Записать по правилам изучаемого языка программирования следующие вы-
ражения
*/
package Task01N017;
import java.util.Scanner;

public class Task01N017s {

    public static void main (String[] args) {
        System.out.println("Решение уравнения вида y=abs(x)+abs(x+1)");
        Scanner scanner = new Scanner(System.in);
        System.out.println ("Введите значения x, целое число: ");
        int x = scanner.nextInt();
        double y = Math.abs(x)+Math.abs(x+1);

        System.out.print("Ответ y = "+y);
    }
}