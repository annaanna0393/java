/*
 В некоторых языках программирования (например, в Паскале) не предусмотрена операция возведения в степень. Написать рекурсивную функцию для расчета степени n вещественного числа a (n — натуральное число)
 */
package Task10N042;
import java.util.Scanner;

public class Task10N042 {
  public static void main(String [] args){
    System.out.print("Bвод натурального числа a и степени n - ");
    Scanner sr = new Scanner(System.in);
    int a = sr.nextInt();
    int n = sr.nextInt();


    System.out.println(a+" в степени "+n+" равно "+result(a, n));
  }
  public static int result (int a,int n)
  {

    if (n==0)
      return 1;
  if (n==1)
    return a;
    else
  {
    return a*result(a, n-1);
  }
}
}