package Task12N234;
import java.util.Scanner;

public class Task12N234a {
  public static void main (String args []) {
    Scanner sr = new Scanner(System.in);
    System.out.println("Ввод n - колличества строк и m - колличества столбцов: ");
    int n = sr.nextInt(); //строки
    int m = sr.nextInt(); //столбцы
    int matr[][] = new int[n][m];
    int i, j, l = 0;
    for (i = 0; i < n; i++)
      for (j = 0; j < m; j++) {
        matr[i][j] = l;
        l++;
      } // Для проверки работы создано автозаполнение.

    for (i = 0; i < n; i++) {
      for (j = 0; j < m; j++) {
        System.out.print(matr[i][j] + "\t");
      }
      System.out.println();
    }

    System.out.println("Ввод k - номера строки для удаления: ");
    int k = sr.nextInt();
    if (k <=n&&k>=1) {

      for (i = 0; i < n; i++)
        if (i == k - 1) {
          int result = 0;
          for (i = k - 1; i < n; i++) {
            if (i < n - 1) {
              for (j = 0; j < m; j++) {
                result = matr[i + 1][j];
                matr[i][j] = result;
              }
            }
            else {
              for (j = 0; j < m; j++)
              matr[i][j] = 0;
            }
          }
        }
      for (i = 0; i < n; i++) {
        for (j = 0; j < m; j++) {
          System.out.print(matr[i][j] + "\t");
        }
        System.out.println();
      }
    }

    else System.out.println("Такой строки не существует");
  }
}