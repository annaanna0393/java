/*
Написать рекурсивную функцию для вычисления факториала натурального
числа n.
 */
package Task10N041;

public class Task10N041 {
  public static void main(String[] args) {
    int n = 10;
    System.out.print("Факториал " + n + " равен " + fact(n));
  }
  public static int fact(int n) {
    int result;
    if (n == 1) {
      return 1;
    }
    result = fact(n - 1) * n;
    return result;
  }
}
