/*
Записать условие, которое является истинным, когда:
д) только одно из чисел X, Y и Z кратно пяти;
 */
package Task03N029;
import java.util.Scanner;

public class Task03N029e {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите числа x, y и z: ");
        double x = scanner.nextInt();
        double y = scanner.nextInt();
        double z = scanner.nextInt();
        double r = x % 5;
        double q = y % 5;
        double w = z % 5;
        if (r == 0) {
            if (q == 0) {
                if (w == 0) {
                    System.out.println("Истина");
                }
                else{
                    System.out.println ("Ложь");
                }
            }
            else{
                System.out.println ("Ложь");
            }
        }
        else{
            System.out.println ("Ложь");
        }
    }
}
