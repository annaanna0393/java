/*
 "Странный муж" . Некий мужчина отправляется на работу, которая находится на расстоянии 1 км от дома. Дойдя до места работы, он вдруг вспоминает, что перед уходом забыл поцеловать жену, и поворачивает назад. Пройдя полпути, он меняет решение, посчитав, что правильнее вернуться на работу. Пройдя 13 км по направлению к работе, он вдруг осознает, что будет настоящим подлецом, если так и не поцелует жену. На этот раз, прежде чем изменить мнение, он проходит 14 км. Так он продолжает метаться, и после Nэтапа, пройдя 1/N км, снова меняет решение. Определить: а) на каком расстоянии от дома будет находиться мужчина после 100-го этапа (если допустить, что такое возможно);
 */
package Task05N038;

public class Task05N038a {
  public static void main (String args []) {
    double b = 1;
    for (double n=2; n<=100; n++){
      double k=n%2;
      if (k==0)
      b = b-1/n;
      else b = b+1/n;
    }
    System.out.println("Мужчина будет на расстоянии "+b+" км от дома");
  }
}
