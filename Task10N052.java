/*
Написать рекурсивную процедуру для вывода на экран цифр натурального числа в обратном порядке
 */
package Task10N052;
import java.util.Scanner;

public class Task10N052 {
  public static void main(String[] args) {
    int n;
    System.out.print("Bвод натурального числа - ");
    Scanner sr = new Scanner(System.in);
    n = sr.nextInt();

    System.out.println("Сумма цифр в числе равна " + result(n, 0));
  }
    public static int result(int n, int i) {
      return (n==0) ? i : result( n/10, i*10 + n%10 );
    }

}
