/*
 Написать рекурсивную функцию, определяющую, является ли заданное натуральное число простым (простым называется натуральное число, боль- шее 1, не имеющее других делителей, кроме единицы и самого себя).
 */

package Task10N056;
import java.util.Scanner;

public class Task10N056 {
  public static void main(String[] args) {
    System.out.print("Bвод числа: ");
    Scanner sr = new Scanner(System.in);
    int n = sr.nextInt();
    System.out.println(recursion(n, 2));
  }

  public static boolean recursion(int n, int i) {
        if (n < 2) {
      return false;
    }
    else if (n == 2) {
      return true;
    }
    else if (n % i == 0) {
      return false;
    }
    else if (i < n / 2) {
      return recursion(n, i + 1);
    } else {
      return true;
    }
  }
}