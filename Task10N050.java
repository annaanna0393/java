/*
 Написать рекурсивную функцию для вычисления значения так называемой функции Аккермана для неотрицательных чисел n и m. Функция Аккермана определяется следующим образом:
1, если 0, , 1, 1 , если 0, 0, 1, , 1 , если 0, 0. mn A n m A n n m A n A n m n m
Функцию Аккермана называют дважды рекурсивной, т. к. сама функция и один из ее аргументов определены через самих себя. Найти значение функции Аккермана для 1, 3. nm
 */
package Task10N050;
public class Task10N050 {
  public static void main(String[] args) {
    int m = 3;
    int n = 1;
    System.out.println(recursion(n, m));
      }
  public static int recursion(int n, int m)
  {
    // Базовый случай
    if (n == 0) {
      return m + 1;
    } // Шаг рекурсии / рекурсивное условие
    else if (m == 0 && n > 0) {
      return recursion(n-1, 1);
    } // Шаг рекурсии / рекурсивное условие
    else {
      return recursion(n - 1, recursion(n, m - 1));
    }
  }
}
