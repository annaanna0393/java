/*
Написать рекурсивную функцию: б) вычисления количества цифр натурального числа.
 */
package Task10N043;
import java.util.Scanner;

public class Task10N043b {
  public static void main(String [] args){
    int n;
    System.out.print("Bвод натурального числа - ");
    Scanner sr = new Scanner(System.in);
    n = sr.nextInt();

    System.out.println("Колличество цифр в числе равно "+sum(n));
  }
  public static int sum (int n)
  {
    if (Math.abs(n) < 10) {
       return 1;
      }
    else {
     return 1+ sum (n/10);
    }
  }
}
