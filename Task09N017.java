/*
. Дано слово. Верно ли, что оно начинается и оканчивается на одну и ту же
букву?

 */
package Task09N017;

public class Task09N017 {
  public static void main (String [] args) {
    String s = "длинноед";
    if (s.charAt(0)==s.charAt(s.length()-1))
      System.out.println("Истина");
    else
      System.out.println("Ложь");
  }
}
