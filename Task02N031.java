package Task02N031;
import java.util.Scanner;

public class Task02N031 {
    public static void main(String[] args) {
        Scanner k = new Scanner(System.in);
        System.out.println("Введите целое число от 100 до 999: ");
        int n = k.nextInt();// заданное число
        int x = 0;
        if (n>=100&&n<=999) {
                    x = n/100;
                    x *= 10;
                    x += n % 10;
                    x *= 10;
                    x += n /10 % 10;
                System.out.print("x="+x);
        }
        else {
            System.out.println ("Число не соответствует требованиям");
        }
    }
}

