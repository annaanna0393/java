/*
Известны год и номер месяца рождения человека, а также год и номер месяца
сегодняшнего дня (январь — 1 и т. д.). Определить возраст человека (число
полных лет). В случае совпадения указанных номеров месяцев считать, что
прошел полный год.
*/

package Task04N015;
import java.util.Scanner;

public class Task04N015 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите числа x и y: ");
        int x = scanner.nextInt();
        int y = scanner.nextInt();
        int xnew = scanner.nextInt();
        int ynew = scanner.nextInt();
        int z;
        if (y<=12&&y>=1&ynew<=12&&ynew>=1&&x>0&&xnew>0&&xnew>x) {
            if (ynew<y) {
                z = xnew-x-1;
                System.out.println("Человеку "+z+" лет");
            }
            else{
                z = xnew-x;
                System.out.println("Человеку "+z+" лет");
            }

        }
        else{
            System.out.println("Не верный ввод услови");
        }
    }
}