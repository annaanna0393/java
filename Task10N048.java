/*
Написать рекурсивную функцию для вычисления максимального элемента массива из n элементов.
 */
package Task10N048;
import java.util.Scanner;

public class Task10N048 {
  public static void main(String[] args) {
    System.out.print("Bвод n, числа элементов в массиве и элементов массива - ");
    Scanner sr = new Scanner(System.in);
    int n = sr.nextInt();
    double[ ] array = new double[n] ;
    for (int i = 0; i < n; i++)
      array[i] = sr.nextDouble();
    System.out.println("Максимальный элемент массива равен: "+max(array, n-1));
  }

  public static double max(double array [], int n) {
    if (n==0)
      return array[0];
    final double previousMax = max(array, n-1);
    return array[n] > previousMax ? array[n] : previousMax;
  }
}
