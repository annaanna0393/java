/*
Дано целое число k (1 k 365). Определить, каким будет k-й день года: вы-
ходным (суббота и воскресенье) или рабочим, если 1 января — понедельник.
 */
package Task04N067;
import java.util.Scanner;

public class Task04N067 {
  public static void main (String args []){
    Scanner scanner = new Scanner(System.in);
    System.out.println("Введите k-й день года: ");
    int d = scanner.nextInt();

    if (d>=1&&d<=365) {
      if (d % 7 == 0 | d % 6 == 0) System.out.print("Ввведённый день выходной");
      else System.out.print("Введённый день рабочий");
    }
    else System.out.print("Не верный ввод условий");

  }
}