/*
Дано слово, состоящее из четного числа букв. Вывести на экран его первую
половину, не используя оператор цикла.
 */

package Task09N022;

public class Task09N022 {
  public static void main (String [] args) {
    String s = "Определить";
    String a = s.substring(0, s.length()/2);
    System.out.println(a);
  }
}