/*
Дано слово. Вывести на экран его k-й символ.
 */
package Task09N015;
import java.util.Scanner;

public class Task09N015 {

public static void main (String [] args) {
  String s = "длинное";
  System.out.println ("Введите номер символа в слове: ");
  Scanner in = new Scanner(System.in);
  int i =in.nextInt ();
  System.out.println(s.charAt(i-1));
}
}
