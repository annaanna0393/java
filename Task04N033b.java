/*
Дано натуральное число. б) Верно ли, что оно заканчивается нечетной цифрой?
 */
package Task04N033;
import java.util.Scanner;

public class Task04N033b {
  public static void main(String args[]) {
    Scanner scanner = new Scanner(System.in);
    System.out.println("Введите число, которое заканчивается нечётной цифрой: ");
    int a = scanner.nextInt();

    a = (a%10)%2;
    if (a==0) {

      System.out.println("Ложь");
    }
    else {
      System.out.println("Истина");
    }
  }
}
