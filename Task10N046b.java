/*
Даны первый член и знаменатель геометрической прогрессии. Написать рекурсивную функцию: б) нахождения суммы n первых членов прогрессии
*/

package Task10N046;
import java.util.Scanner;
public class Task10N046b {
  public static void main(String [] args){

    System.out.print("Bвод первого члена, знаменателя и номера n-го члена геометрической прогрессии - ");
    Scanner sr = new Scanner(System.in);
    int a = sr.nextInt();
    int q = sr.nextInt();
    int n = sr.nextInt();
    if (q==0|n==0)
    {System.out.println("Не верный ввод значений");}
    else
    {System.out.println("сумма n первых членов прогрессии равна "+result(q, a, n-1));}

  }
  public static int result (int q, int a, int n) {

    if (n==0)
      return a;
    else {
      int s = q*result (q, a, n-1);
      return a+s;
    }

  }
}