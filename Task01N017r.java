/*
Записать по правилам изучаемого языка программирования следующие вы-
ражения
*/
package Task01N017;
import java.util.Scanner;

public class Task01N017r {

    public static void main (String[] args) {
        System.out.println("Решение уравнения вида y=(sqrt(x+1)+sqrt(x-1))/(2*sqrt(x)), введите 1≤x");
        Scanner scanner = new Scanner(System.in);
        System.out.println ("Введите значения x, целое число: ");
        double x = scanner.nextDouble();
        double z = x-1;
        if (z>=0)
        {
            double y = (Math.sqrt(x+1)+Math.sqrt(x-1))/(2*Math.sqrt(x));
            System.out.print("Ответ y = "+y);
        }
        else {
            System.out.print("Не верный ввод условий");
        }
    }
}