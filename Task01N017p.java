/*
Записать по правилам изучаемого языка программирования следующие вы-
ражения
*/
package Task01N017;
import java.util.Scanner;

public class Task01N017p {

    public static void main (String[] args) {
        System.out.println("Решение уравнения вида y=1/sqrt(a*x^2+b*x+c)");
        Scanner scanner = new Scanner(System.in);
        System.out.println ("Введите значения x, a, b, c, целые числа: ");
        double x = scanner.nextDouble();
        double a = scanner.nextDouble();
        double b = scanner.nextDouble();
        double c = scanner.nextDouble();
        double z = a*Math.pow(x, 2)+b*x+c;

        if (z>=0) {
            double y = 1 / Math.sqrt(z);
            System.out.print("Ответ y = " + y);
        }
        else {
            System.out.print("Не верный ввод условий");
        }
    }
}