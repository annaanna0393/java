/*
Даны первый член и разность арифметической прогрессии. Написать рекурсивную функцию для нахождения: а) n-го члена прогрессии;

 */
package Task10N045;

import java.util.Scanner;

public class Task10N045a {
  public static void main(String [] args){

    System.out.print("Bвод первого члена, разности и номера n-го члена арифметической прогрессии - ");
    Scanner sr = new Scanner(System.in);
    int a = sr.nextInt();
    int d = sr.nextInt();
    int n = sr.nextInt();
    if (d==0|n==0)
    { System.out.println("Не верный ввод значений");}
    else
    {System.out.println("n-ый член прогрессии равен "+result(d, a, n-1));}
  }
  public static int result (int d, int a, int n) {

    if (n==0)
      return a;
    else {
      return d+result (d, a, n-1);
    }
  }
}