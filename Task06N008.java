/*
 Дано число n. Из чисел 1, 4, 9, 16, 25, ... напечатать те, которые не превышают n.
 */
package Task06N008;
import java.util.Scanner;

public class Task06N008 {
  public static void main (String args []) {
    System.out.print("Bвод натурального числа - ");
    Scanner sr = new Scanner(System.in);
    int n = sr.nextInt();
    int i;
    if (n>0)
    { System.out.println("Из чисел 1, 4, 9, 16, 25, ... , не превышают "+n+" ");
    for (i=1; i*i<n; i++) {
      System.out.println(i*i);}
    }
    else System.out.println("Из чисел 1, 4, 9, 16, 25, ... , все меньше "+n);
  }
}