package Task05N010;

import java.util.Scanner;

public class Task05N010 {
  public static void main (String args []) {
    System.out.println("Введите курс: ");
    Scanner in = new Scanner(System.in);
    double e = in.nextDouble();
    for (int n=1; n<=20; n++)
    {
      double m = e*n;
      System.out.println(n+" долларов по курсу "+e+" будет "+m+" рублей.");
    }
  }
}