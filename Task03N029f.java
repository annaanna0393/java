/*
Записать условие, которое является истинным, когда:
        е) хотя бы одно из чисел X, Y, Z больше 100.
        */
        package Task03N029;
        import java.util.Scanner;

public class Task03N029f {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите числа x и y: ");
        double x = scanner.nextInt();
        double y = scanner.nextInt();
        double z = scanner.nextInt();
        if (x>100) {
            System.out.println ("Истина");
        }
        else{

            if (y>100)
            {
                System.out.println ("Истина");
            }
            else
            {
                if (z>100) {
                    System.out.println("Истина");
                }
                else {
                    System.out.println("Ложь");
                }
            }
        }
    }
}
